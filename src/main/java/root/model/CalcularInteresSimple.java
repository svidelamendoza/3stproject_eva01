/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model;


/**
 *
 * @author Sthephania
 */
public class CalcularInteresSimple {
    

       
    public double getCalcularInteresSimple (String capital, String periodo, String tasa){
        
        double capitalNum = Double.parseDouble(capital);
        int periodoNum = Integer.parseInt(periodo);
        int tasaNum = Integer.parseInt(tasa);
         
        double intCalculado = capitalNum * (tasaNum * 0.01) * periodoNum;
        
    

        return intCalculado;
        
    }
    
}

