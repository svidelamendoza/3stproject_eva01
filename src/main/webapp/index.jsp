<%-- 
    Document   : index
    Created on : 06-04-2020, 1:14:36
    Author     : Sthephania
--%>
<%@page import="java.lang.String"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CIISA-Eva01</title>
  </head>
  <body>
                   
        <!-- Default form ingreso de datos -->
    <form class="text-center border border-light p-5" action="controller" method="POST">

    <p class="h4 mb-4">Calculadora de Interés Simple</p>

    <p>Ingrese los datos solicitados a continuación</p>

    <!-- Rut -->
  	Ingresa tu Rut:
    <input type="number" name="rut" value="" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="12345678-9">
   
    <!--Nombre-->
  	Indica tu nombre y apellido:
    <input type="text" name="nombre" value="" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="Juan Pérez">

    <!--Capital-->
  	Indica tu capital inicial:
    <input type="text" name="capital" value="" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="200.000">
  
    <!--Capital-->
  	Indica tu tasa de interés anual: (ejemplo: 2%)
    <input type="number" name="tasa" value="" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="ingresa sólo números">
    </select>
   <!--Período-->
  	Indica periodo en años:
    <input type="number" name="periodo" value="" id="defaultSubscriptionFormPassword" class="form-control mb-4" placeholder="ingresa sólo números">


    <!-- botones submit -->
    <button <input class="btn btn-info btn-block" type="submit">Calcular Interés</button>
     <p></p>
    <button <input class="btn btn-info btn-block" type="reset">Borrar Datos</button>
              
        </form>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>
