<%-- 
    Document   : salida
    Created on : 06-04-2020, 1:15:03
    Author     : Sthephania
--%>

<%@page import="java.lang.String"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CIISA-Eva01</title>
    </head>
   
    <%
            double intCalculado = (Double) request.getAttribute("interesCalculado");
            String nombre = (String) request.getAttribute("nombre");
            String rut = (String) request.getAttribute("rut");
            String capital = (String) request.getAttribute("capital");
            String periodo = (String) request.getAttribute("periodo");
            String tasa = (String) request.getAttribute("tasa");
            
        %>
        <body>
         
            
            <div class="container">

                <!-- Content here -->
  
  <p class="h4 mb-4">Resultado de tus datos ingresados </p>
  
        <p>Rut: <%=rut%></p>
        <p>Nombre: <%=nombre%></p>
        <p>Capital Inicial: <%=capital%></p>
        <p>Tasa Interés Anual: <%=tasa%></p>
        <p>Período Años: <%=periodo%></p>
        <p>Monto Interés Calculado (I):<%= intCalculado%></p>
          
    <!-- botón para volver -->
    <form action="index.jsp" method="POST">
    <button <input class="btn btn-info btn-block" type=""; >Volver a Calcular </button>
     </form>   
        <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
           </body> 
</html>
